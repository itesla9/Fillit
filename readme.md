***
Your executable must take as parameter one (and only one) file which contains a
list of Tetriminos to arrange. This file has a very specific format : every Tetriminos
description consists of 4 lines and each Tetriminos are **separated by an empty line**

If the number of parameters given to your executable is different from 1, your program
must display his usage and exit properly. If you dont know what a usage is, execute the
command cp without arguments in your Shell to get an idea.

The description of a Tetriminos must respect the following rules :
---
<li> 
* Precisely 4 lines of 4 characters followed by a new line.
* A Tetriminos is a classic piece of Tetris composed of 4 blocks.
* Each character must be either a ’#’ when the character is one of the 4 blocks of
a Tetriminos or a ’.’ if it’s empty.
* Each block of a Tetriminos must be in contact with at least one other block on
any of his 4 sides.
A few examples of valid descriptions of Tetriminos :

```
.... .... #### .... .##. .... .#.. .... ....
..## .... .... .... ..## .##. ###. ##.. .##.
..#. ..## .... ##.. .... ##.. .... #... ..#.
..#. ..## .... ##.. .... .... .... #... ..#.

```
A few examples of invalid descriptions of Tetriminos
---
```
#### ...# ##... #.. .... ..## #### ,,,, .HH.
...# ..#. ##... ##. .... .... #### #### HH..
.... .#.. ....  #.. .... .... #### ,,,, ....
.... #... .... .... ##.. #### ,,,, .... ....
```

Because each Tetriminos occupies only 4 of the 16 available boxes, it is possible to
describe the same Tetrimino in multiple ways. However, the rotation of a Tetrimino
describes a different Tetrimino from the original in the case of this project. This means
that no rotation is possible on a Tetrimino, when you will arrange it with the others.
Those Tetriminos are then perfectly equivalents on every aspect :

```
##.. .##. ..## .... .... ....
#... .#.. ..#. ##.. .##. ..##
#... .#.. ..#. #... .#.. ..#.
.... .... .... #... .#.. ..#.
```
These 5 Tetriminos are, for their part, 5 entirely distinct Tetriminos on every aspect
:
```
##.. .### .... .... ....
#... ...# ...# .... .##.
#... .... ...# #... .##.
.... .... ..## ###. ....
```
To finish, here is an example of a valid file your program must resolve :

```
$> cat -e valid_sample.fillit
...#$
...#$
...#$
...#$
$
....$
....$
....$
####$
$
.###$
...#$
....$
....$
$
....$
..##$
.##.$
....$
$>
```